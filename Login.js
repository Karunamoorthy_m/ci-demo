import React, { Component } from 'react';
import { Dimensions, SafeAreaView, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import Colors from './Colors';
import CustomModule from './CustomModule';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  toastButtonClick = () => {
    console.log('Result', this.state);
    CustomModule.signIn(this.state.email, this.state.password);
  };

  onEmailChange = event => {
    const {text} = event.nativeEvent;
    this.setState({email: text});
  };

  onPasswordChange = event => {
    const {text} = event.nativeEvent;
    this.setState({password: text});
  };

  render() {
    return (
      <SafeAreaView style={styles.innerContainer}>
        <StatusBar backgroundColor={Colors.background} />
        <TextInput
          style={styles.textInput}
          numberOfLines={1}
          placeholderTextColor={Colors.semiWhite}
          placeholder={'Enter your E-Mail'}
          keyboardType={'email-address'}
          selectionColor={Colors.white}
          onChange={this.onEmailChange}
        />

        <TextInput
          secureTextEntry={true}
          style={[{marginTop: 24}, styles.textInput]}
          numberOfLines={1}
          placeholderTextColor={Colors.semiWhite}
          placeholder={'Enter your Password'}
          selectionColor={Colors.white}
          onChange={this.onPasswordChange}
        />

        <TouchableOpacity
          onPress={this.toastButtonClick}
          style={{
            width: '50%',
            height: 48,
            backgroundColor: Colors.borderColor,
            borderRadius: 8,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 36,
          }}>
          <Text style={{color: 'white', fontSize: 18}}>Sign In</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

export default Login;

const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  innerContainer: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.background,
  },
  titleText: {
    fontSize: (16 / width) * width,
    color: '#00f',
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    height: (40 / height) * height,
  },
  textInput: {
    height: 48,
    width: '70%',
    borderColor: Colors.borderColor,
    borderRadius: 8,
    color: Colors.white,
    borderWidth: 1,
    paddingLeft: 8,
    fontSize: 18,
  },
});
