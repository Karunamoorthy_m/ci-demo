export default Colors = {
  white: 'white',
  semiWhite:'#ffffff80',
  background:'#160f26',
  borderColor:'#D600FF'
};
