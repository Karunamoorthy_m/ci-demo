package com.ci_react_native;

import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.HashMap;
import java.util.Map;

public class CustomModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;

    private static final String USER_NAME_KEY = "USER_NAME";
    private static final String PASSWORD_KEY = "PASSWORD";


    CustomModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "CustomModule";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(USER_NAME_KEY, "");
        constants.put(PASSWORD_KEY, "");
        return constants;
    }

    @ReactMethod
    public void signIn(String userName, String password) {
        Log.e("Username", userName);
        Log.e("password", password);

        String message = "Email & password is not valid.";

        if (Utils.isValidEmail(userName) && Utils.isValidPassword(password)) {
            message = "Email & password is valid.";
        }

        Toast.makeText(getReactApplicationContext(), message, Toast.LENGTH_LONG).show();

    }
}

