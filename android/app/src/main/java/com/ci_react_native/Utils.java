package com.ci_react_native;

import java.util.regex.Pattern;

public class Utils {

    public static boolean isValidEmail(String email) {
        if (email == null)
            return false;
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        return Pattern.compile(emailRegex).matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        return password != null && password.length() > 5;
    }
}


