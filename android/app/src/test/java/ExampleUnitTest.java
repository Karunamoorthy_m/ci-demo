import com.ci_react_native.Utils;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testEmailAndPassword(){
        assertTrue(Utils.isValidEmail("name@email.com"));
        assertTrue(Utils.isValidPassword("wewerwee"));
    }
}